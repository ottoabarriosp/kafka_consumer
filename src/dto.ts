export class MessageGetDTO {
  timestamp: Date;
  topic: string;
  partition: number;
  producer: string;
  key: string;
  data: any;

  constructor({ timestamp, topic, partition, key, value }: any) {
    this.timestamp = new Date(timestamp);
    this.topic = topic;
    this.partition = partition;
    this.key = key;
    this.producer = value.producer;
    this.data = value.message;
  }
}
