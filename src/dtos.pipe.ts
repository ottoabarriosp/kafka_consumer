import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import { MessageGetDTO } from './dto';

@Injectable()
export class ConvertToDtoPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata): MessageGetDTO {
    return new MessageGetDTO(value);
  }
}
