import { SchemaRegistry } from '@kafkajs/confluent-schema-registry';
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
} from '@nestjs/common';
import { RpcArgumentsHost } from '@nestjs/common/interfaces';
import { Observable } from 'rxjs';

@Injectable()
export class SerializerInterceptor implements NestInterceptor {
  private readonly logger = new Logger(SerializerInterceptor.name);
  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    try {
      const host: RpcArgumentsHost = context.switchToRpc();
      const data: any = host.getData();

      data.timestamp = new Date(Number(data.timestamp) * 1000).toISOString();

      const registry = new SchemaRegistry({
        host: 'http://localhost:8081',
      });
      const buffer = Buffer.from(data.value);

      data.value = await registry.decode(buffer);
    } catch (err) {
      this.logger.error(err.message || err, err.stack);
    }

    return next.handle().pipe((value) => {
      return value;
    });
  }
}
