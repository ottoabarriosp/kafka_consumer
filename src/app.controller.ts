import { Controller, Logger, UseInterceptors, UsePipes } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';
import { SerializerInterceptor } from './serializer.interceptor';
import { ConvertToDtoPipe } from './dtos.pipe';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  private readonly logger = new Logger(AppController.name);

  @MessagePattern('test')
  getTest(@Payload() message: any): string {
    this.logger.debug('[Test Consumer]');
    console.log(message.value);
    return this.appService.getHello();
  }

  @UseInterceptors(SerializerInterceptor)
  @UsePipes(ConvertToDtoPipe)
  @MessagePattern('actions')
  async getActions(@Payload() message: any): Promise<string> {
    this.logger.debug('[Actions Consumer]');
    console.log(message);
    return this.appService.getHello();
  }

  @UseInterceptors(SerializerInterceptor)
  @UsePipes(ConvertToDtoPipe)
  @MessagePattern('products')
  async getProducts(@Payload() message: any): Promise<any> {
    this.logger.debug('[Products Consumer]');
    console.log(message);
    return this.appService.getHello();
  }

  @UseInterceptors(SerializerInterceptor)
  @UsePipes(ConvertToDtoPipe)
  @MessagePattern('errors')
  async getErrors(@Payload() message: any): Promise<string> {
    this.logger.debug('[Errors Consumer]');
    console.log(message);
    return this.appService.getHello();
  }

  @UseInterceptors(SerializerInterceptor)
  @UsePipes(ConvertToDtoPipe)
  @MessagePattern('jobdata')
  async getJobdata(@Payload() message: any): Promise<string> {
    this.logger.debug('[Jobdata Consumer]');
    console.log(message);
    return this.appService.getHello();
  }

  @UseInterceptors(SerializerInterceptor)
  @UsePipes(ConvertToDtoPipe)
  @MessagePattern('jobs')
  async getCronJobs(@Payload() message: any): Promise<string> {
    this.logger.debug('[Jobdata Consumer]');
    console.log(message);
    return this.appService.getHello();
  }
}
